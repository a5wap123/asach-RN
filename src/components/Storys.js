//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button,ListView,TouchableHighlight,Alert } from 'react-native';
import { connect } from 'react-redux';
import { fetchData } from '../actions';
// create a component
class Storys extends Component {
    render() {
        console.log(this.props)
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        const {isFetStoryching, isFetStoryched, StoryError, eMgs, storyState} = this.props.dataStory;
        return (
            <View style={styles.container}>
                <Button title='Get Story' onPress={
                    () => { this.props.fetchData(0,5) }
                } />
                {
                    isFetStoryching && <Text>Loading..</Text>

                }
                {
                    StoryError && <Text>{eMgs}</Text>
                }
                {
                    
                    isFetStoryched && 
                    <View style={{ flex: 1, paddingTop: 20, alignItems: 'center' }}>
                        <ListView
                            dataSource={ds.cloneWithRows(storyState)}
                            renderRow={(rowData) =>
                                <View style={{ padding: 5 }}>
                                    <Text>{rowData.NameStory}</Text>
                                    <TouchableHighlight style={{ padding: 5, backgroundColor: 'green', borderRadius: 10 }}
                                        onPress={() => {
                                            Alert.alert(
                                                `${rowData.Cover}`,
                                                `${rowData.DateUpdate}`, [
                                                    { text: 'Cancel' },
                                                    { text: 'OK' }
                                                ]
                                            )
                                        }}
                                    >
                                        <Text style={{ textAlign: 'center' }}>Đoc sách</Text>
                                    </TouchableHighlight>
                                </View>
                            }
                        />
                    </View>
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});
const mapStateToProp = (state) => {
    return {
        dataStory: state.storyReducer
    }
}
//make this component available to the app
export default connect(mapStateToProp, { fetchData })(Storys);
