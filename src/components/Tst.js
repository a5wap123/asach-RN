import React, { Component } from 'react';
import { ActivityIndicator, ListView, Text, View,Image } from 'react-native';

export default class Tst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {
    return fetch('https://truyenhay.herokuapp.com/api/storys?start=0&limit=5')
      .then((response) => response.json())
      .then((responseJson) => {
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
          isLoading: false,
          dataSource: ds.cloneWithRows(responseJson.data),
        }, function() {
          // do something with new state
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View style={{flex: 1, paddingTop: 20}}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => <View style={{flexDirection:'row'}}>
              <Image style={{width:90,height:120}} source={{uri:rowData.Cover}} />
              <Text>{rowData.NameStory}</Text>
              </View>}
        />
      </View>
    );
  }
}