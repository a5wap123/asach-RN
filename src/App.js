//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {Provider} from 'react-redux';
import store from './configStore';
import Storys from './components/Storys';
import Tst from './components/Tst';
// create a component
class App extends Component {
    render() {
        return (
           <Provider store={store}>
            <View style={styles.container}>
             <Tst/> 
            </View>
           </Provider>
              
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default App;
