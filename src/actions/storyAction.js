//STORY
export const FETCH_STORY_OK = 'FETCH_STORY_OK';
export const FETCH_STORY_FAIL = 'FETCH_STORY_FAIL';
export const FETCHING_STORY = 'FETCHING_STORY';

import {getStorys} from '../apis/api';

export const getData = () => {
    return {
        type: FETCHING_STORY
    }
}
export const getDataSuccess = (data) => {
    return {
        type: FETCH_STORY_OK,
        payload: data
    }
}
export const getDataFail = (eMgs) => {
    return {
        type: FETCH_STORY_FAIL,
        eMgs: eMgs
    }
}

export const fetchData = (start,limit) => {
    return (dispatch) => {
        dispatch(getData());
        getStorys(start,limit).then((data)=>{
            console.log(data);
            dispatch(getDataSuccess(data.data));
        }).catch((e)=>{
            console.error(e);
            getDataFail(e);
        })
    }
}
