export const Domain = 'https://truyenhay.herokuapp.com';
export const apiChaper = '/api/chaper';
export const apiStory2Type = '/api/story2type';
export const apiStory = '/api/story';
export const apiStorys = '/api/storys';
export const apiAuthor = '/api/author';