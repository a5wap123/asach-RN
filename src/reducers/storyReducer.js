import {FETCH_STORY_OK,FETCH_STORY_FAIL,FETCHING_STORY} from '../actions';

const DEFAULT_STATE ={
    dataState:[],
    eMgs: '',
    isFetStoryching: false,
    isFetStoryched: false,
    error: false
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case FETCHING_STORY:
            return {
                ...state,
                isFetStoryching: true
            };
            case FETCH_STORY_OK:
            return{
                ...state,
                dataState: action.payload,
                isFetStoryched: true,
                isFetching: false
            };
            case FETCH_STORY_FAIL:
            return { 
                ...state,
                isFetStoryching: false,
                error: true,
                eMgs: action.eMgs
                
            };
        default:
            return state;
    }
};
