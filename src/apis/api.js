import {Domain,apiStorys} from '../servers/config';

export const getStorys = (start,limit) => {
    var url = `${Domain}${apiStorys}?start=${start}&limit=${limit}`;
    return fetch(url);
}
